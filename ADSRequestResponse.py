import json
import requests

to_resp_parse = []
from_resp_parse = []

missing_in_to_env = []
mismatch_in_to_env = []
missing_in_from_env = []

from_resp = {"pod":{'id': 2257, 'number': 0, 'unit_number': 1, 'group_list': [{'id': 68058, 'service_name': 'gap', 'deployment_mode': 'server', 'deploy_by': 0, 'restart_by': 1, 'service_list': [{'id': 402312, 'name': 'aws11-g07-gap01', 'domain': 'dev.glip.net', 'port': 443, 'deployment_type': 'Auto', 'in_pool': True, 'in_pool_deployed': True, 'state': 'started', 'release': 'GAP', 'monitoring_version_tag': 'GAP_v1', 'deployed_release': {'name': '', 'branch': 'release-2.4.0-master', 'build': '2021-08-09_17-54-09', 'subcomponents': {}}, 'configured_release': {'name': 'GAP', 'branch': 'release-2.4.0-master', 'build': '2021-08-09_17-54-09', 'subcomponents': {}}, 'deployed_monitoring_version_tag': 'GAP_v1'}], 'number': 1}, {'id': 45398, 'service_name': 'gas', 'deployment_mode': 'server', 'deploy_by': 0, 'restart_by': 1, 'service_list': [{'id': 355637, 'name': 'aws11-g07-uds05', 'domain': 'dev.glip.net', 'port': 443, 'deployment_type': 'Auto', 'in_pool': True, 'in_pool_deployed': True, 'state': 'started', 'release': 'glp-ci-demo_nodejs', 'monitoring_version_tag': 'GAS_v17', 'deployed_release': {'name': '', 'branch': 'GLIP-32768', 'build': '2021-11-17_17-56-36_d353012f1b29ea13', 'subcomponents': {}}, 'configured_release': {'name': 'glp-ci-demo_nodejs', 'branch': None, 'build': '- Latest -', 'subcomponents': {}}, 'deployed_monitoring_version_tag': 'GAS_v17'}], 'number': 1}, {'id': 45399, 'service_name': 'gcs', 'deployment_mode': 'server', 'deploy_by': 0, 'restart_by': 1, 'service_list': [{'id': 355638, 'name': 'aws11-g07-uds05', 'domain': 'dev.glip.net', 'port': 443, 'deployment_type': 'Auto', 'in_pool': True, 'in_pool_deployed': True, 'state': 'started', 'release': 'glp-ci-demo_nodejs', 'monitoring_version_tag': 'GCS_v7', 'deployed_release': {'name': '', 'branch': 'GLIP-32768', 'build': '2021-11-17_17-56-36_d353012f1b29ea13', 'subcomponents': {}}, 'configured_release': {'name': 'glp-ci-demo_nodejs', 'branch': None, 'build': '- Latest -', 'subcomponents': {}}, 'deployed_monitoring_version_tag': 'GCS_v7'}], 'number': 1},{'id': 143668, 'service_name': 'glpin', 'deployment_mode': 'external', 'deploy_by': 1, 'restart_by': 1, 'service_list': [{'id': 2346590, 'name': 'glp01-g02-gin01', 'domain': 'glp-ft2-aws.svc.c01.eks01.k8s.aws11.dev.glip.net', 'port': 3014, 'deployment_type': 'Auto', 'in_pool': True, 'in_pool_deployed': True, 'state': 'started', 'release': 'GLP-FT2-AWS-glpin-1', 'deployed_release': {'name': '', 'branch': 'develop', 'build': '2021-08-03_08-40-04-47b4b31', 'subcomponents': {}}, 'configured_release': {'name': 'GLP-FT2-AWS-glpin-1', 'branch': 'develop', 'build': '2021-08-03_08-40-04-47b4b31', 'subcomponents': {}}}], 'number': 1}, {'id': 143698, 'service_name': 'glpmb', 'deployment_mode': 'server', 'deploy_by': 0, 'restart_by': 1, 'service_list': [{'id': 2348759, 'name': 'glp01-g02-gmb02', 'domain': 'dev.glip.net', 'port': 3000, 'deployment_type': 'Auto', 'in_pool': True, 'in_pool_deployed': True, 'state': 'started', 'release': 'GLP-FT2-AWS-glpmb-1', 'monitoring_version_tag': 'GMB_v1', 'deployed_release': {'name': '', 'branch': 'release-1.2.0', 'build': '2020-11-05_15-56-45-v1.2.0', 'subcomponents': {}}, 'configured_release': {'name': 'GLP-FT2-AWS-glpmb-1', 'branch': 'release-1.2.0', 'build': '2020-11-05_15-56-45-v1.2.0', 'subcomponents': {}}, 'deployed_monitoring_version_tag': 'GMB_v1'}], 'number': 1}, {'id': 143666, 'service_name': 'glpw', 'deployment_mode': 'external', 'deploy_by': 1, 'restart_by': 1, 'service_list': [{'id': 2346588, 'name': 'glp01-g02-gpw01', 'domain': 'glp-ft2-aws.svc.c01.eks01.k8s.aws11.dev.glip.net', 'port': 3018, 'deployment_type': 'Auto', 'in_pool': True, 'in_pool_deployed': True, 'state': 'started', 'release': 'GLP-FT2-AWS-glpw-2', 'deployed_release': {'name': '', 'branch': 'develop', 'build': '2021-08-03_11-22-19-2de83ae', 'subcomponents': {}}, 'configured_release': {'name': 'GLP-FT2-AWS-glpw-2', 'branch': 'develop', 'build': '2021-08-03_11-22-19-2de83ae', 'subcomponents': {}}}], 'number': 2}, {'id': 45401, 'service_name': 'gns', 'deployment_mode': 'server', 'deploy_by': 0, 'restart_by': 1, 'service_list': [{'id': 355640, 'name': 'aws11-g07-uds05', 'domain': 'dev.glip.net', 'port': 80, 'deployment_type': 'Auto', 'in_pool': True, 'in_pool_deployed': True, 'state': 'started', 'release': 'glp-ci-demo_nodejs', 'monitoring_version_tag': 'GNS_v12', 'deployed_release': {'name': '', 'branch': 'GLIP-32768', 'build': '2021-11-17_17-56-36_d353012f1b29ea13', 'subcomponents': {}}, 'configured_release': {'name': 'glp-ci-demo_nodejs', 'branch': None, 'build': '- Latest -', 'subcomponents': {}}, 'deployed_monitoring_version_tag': 'GNS_v12'}], 'number': 1}, {'id': 45402, 'service_name': 'gps', 'deployment_mode': 'server', 'deploy_by': 0, 'restart_by': 1, 'service_list': [{'id': 355641, 'name': 'aws11-g07-uds05', 'domain': 'dev.glip.net', 'port': 80, 'deployment_type': 'Auto', 'in_pool': True, 'in_pool_deployed': True, 'state': 'started', 'release': 'glp-ci-demo_nodejs', 'monitoring_version_tag': 'GPS_v12', 'deployed_release': {'name': '', 'branch': 'GLIP-32768', 'build': '2021-11-17_17-56-36_d353012f1b29ea13', 'subcomponents': {}}, 'configured_release': {'name': 'glp-ci-demo_nodejs', 'branch': None, 'build': '- Latest -', 'subcomponents': {}}, 'deployed_monitoring_version_tag': 'GPS_v12'}], 'number': 1}, {'id': 45403, 'service_name': 'gss', 'deployment_mode': 'server', 'deploy_by': 0, 'restart_by': 1, 'service_list': [{'id': 135002, 'name': 'aws11-g07-uds06', 'domain': 'dev.glip.net', 'port': 9200, 'deployment_type': 'Undeploy', 'in_pool': False, 'in_pool_deployed': True, 'state': 'started', 'deployed_release': {'name': '', 'branch': '3.0.1639-ads', 'build': '2016-10-11_13-44-06', 'subcomponents': {}}}], 'number': 1}, {'id': 143699, 'service_name': 'gst', 'deployment_mode': 'server', 'deploy_by': 0, 'restart_by': 1, 'service_list': [{'id': 2348760, 'name': 'glp01-g02-gst02', 'domain': 'dev.glip.net', 'port': 8080, 'deployment_type': 'Auto', 'in_pool': True, 'in_pool_deployed': True, 'state': 'started', 'release': 'GLP-FT2-AWS-gst-1', 'monitoring_version_tag': 'GST_v1', 'deployed_release': {'name': '', 'branch': 'release-1.0.x', 'build': '2020-08-27_18-08-02-v1.0.5', 'subcomponents': {}}, 'configured_release': {'name': 'GLP-FT2-AWS-gst-1', 'branch': 'release-1.0.x', 'build': '2020-08-27_18-08-02-v1.0.5', 'subcomponents': {}}, 'deployed_monitoring_version_tag': 'GST_v1'}], 'number': 1}, {'id': 45404, 'service_name': 'gsx', 'deployment_mode': 'server', 'deploy_by': 0, 'restart_by': 1, 'service_list': [{'id': 355642, 'name': 'aws11-g07-uds05', 'domain': 'dev.glip.net', 'port': 443, 'deployment_type': 'Auto', 'in_pool': True, 'in_pool_deployed': True, 'state': 'started', 'release': 'glp-ci-demo_nodejs', 'monitoring_version_tag': 'GSX_v15', 'deployed_release': {'name': '', 'branch': 'GLIP-32768', 'build': '2021-11-17_17-56-36_d353012f1b29ea13', 'subcomponents': {}}, 'configured_release': {'name': 'glp-ci-demo_nodejs', 'branch': None, 'build': '- Latest -', 'subcomponents': {}}, 'deployed_monitoring_version_tag': 'GSX_v15'}], 'number': 1}, {'id': 45405, 'service_name': 'gzs', 'deployment_mode': 'server', 'deploy_by': 0, 'restart_by': 1, 'service_list': [{'id': 355643, 'name': 'aws11-g07-uds05', 'domain': 'dev.glip.net', 'port': 443, 'deployment_type': 'Auto', 'in_pool': True, 'in_pool_deployed': True, 'state': 'started', 'release': 'glp-ci-demo_nodejs', 'monitoring_version_tag': 'GPS_v12', 'deployed_release': {'name': '', 'branch': 'GLIP-32768', 'build': '2021-11-17_17-56-36_d353012f1b29ea13', 'subcomponents': {}}, 'configured_release': {'name': 'glp-ci-demo_nodejs', 'branch': None, 'build': '- Latest -', 'subcomponents': {}}, 'deployed_monitoring_version_tag': 'REC_v1'}], 'number': 1}, {'id': 45406, 'service_name': 'mgs', 'deployment_mode': 'server', 'deploy_by': 0, 'restart_by': 1, 'service_list': [{'id': 135005, 'name': 'aws11-g07-uds06', 'domain': 'dev.glip.net', 'port': 27017, 'deployment_type': 'Undeploy', 'in_pool': False, 'in_pool_deployed': True, 'state': 'started', 'deployed_release': {'name': '', 'branch': '3.0.1639-ads', 'build': '2016-10-11_13-44-06', 'subcomponents': {}}}], 'number': 1}, {'id': 45407, 'service_name': 'mis', 'deployment_mode': 'server', 'deploy_by': 0, 'restart_by': 1, 'service_list': [{'id': 355644, 'name': 'aws11-g07-uds05', 'domain': 'dev.glip.net', 'port': 443, 'deployment_type': 'Auto', 'in_pool': True, 'in_pool_deployed': True, 'state': 'started', 'release': 'glp-ci-demo_nodejs', 'monitoring_version_tag': 'MIS_V16', 'deployed_release': {'name': '', 'branch': 'GLIP-32768', 'build': '2021-11-17_17-56-36_d353012f1b29ea13', 'subcomponents': {}}, 'configured_release': {'name': 'glp-ci-demo_nodejs', 'branch': None, 'build': '- Latest -', 'subcomponents': {}}, 'deployed_monitoring_version_tag': 'MIS_v15'}], 'number': 1}, {'id': 45408, 'service_name': 'rsm', 'deployment_mode': 'server', 'deploy_by': 0, 'restart_by': 1, 'service_list': [{'id': 135007, 'name': 'aws11-g07-uds06', 'domain': 'dev.glip.net', 'port': 6379, 'deployment_type': 'Undeploy', 'in_pool': False, 'in_pool_deployed': True, 'state': 'started', 'deployed_release': {'name': '', 'branch': '3.0.1639-ads', 'build': '2016-10-11_13-44-06', 'subcomponents': {}}}], 'number': 1}]}}
to_resp = {"pod":{'id': 2257, 'number': 0, 'unit_number': 1, 'group_list': [{'id': 68058, 'service_name': 'gap', 'deployment_mode': 'server', 'deploy_by': 0, 'restart_by': 1, 'service_list': [{'id': 402312, 'name': 'aws11-g07-gap01', 'domain': 'dev.glip.net', 'port': 443, 'deployment_type': 'Auto', 'in_pool': True, 'in_pool_deployed': True, 'state': 'started', 'release': 'GAP', 'monitoring_version_tag': 'GAP_v1', 'deployed_release': {'name': '', 'branch': 'release-2.4.1-master', 'build': '2021-08-09_17-54-09', 'subcomponents': {}}, 'configured_release': {'name': 'GAP', 'branch': 'release-2.4.0-master', 'build': '2021-08-09_17-54-09', 'subcomponents': {}}, 'deployed_monitoring_version_tag': 'GAP_v1'}], 'number': 1}, {'id': 45398, 'service_name': 'gas', 'deployment_mode': 'server', 'deploy_by': 0, 'restart_by': 1, 'service_list': [{'id': 355637, 'name': 'aws11-g07-uds05', 'domain': 'dev.glip.net', 'port': 443, 'deployment_type': 'Auto', 'in_pool': True, 'in_pool_deployed': True, 'state': 'started', 'release': 'glp-ci-demo_nodejs', 'monitoring_version_tag': 'GAS_v17', 'deployed_release': {'name': '', 'branch': 'GLIP-32768', 'build': '2021-11-17_17-56-36_d353012f1b29ea13', 'subcomponents': {}}, 'configured_release': {'name': 'glp-ci-demo_nodejs', 'branch': None, 'build': '- Latest -', 'subcomponents': {}}, 'deployed_monitoring_version_tag': 'GAS_v17'}], 'number': 1}, {'id': 45400, 'service_name': 'gdp', 'deployment_mode': 'server', 'deploy_by': 0, 'restart_by': 1, 'service_list': [{'id': 355639, 'name': 'aws11-g07-uds05', 'domain': 'dev.glip.net', 'port': 443, 'deployment_type': 'Auto', 'in_pool': True, 'in_pool_deployed': True, 'state': 'started', 'release': 'glp-ci-demo_nodejs', 'monitoring_version_tag': 'GDP_v2', 'deployed_release': {'name': '', 'branch': 'GLIP-32768', 'build': '2021-11-17_17-56-36_d353012f1b29ea13', 'subcomponents': {}}, 'configured_release': {'name': 'glp-ci-demo_nodejs', 'branch': None, 'build': '- Latest -', 'subcomponents': {}}, 'deployed_monitoring_version_tag': 'GDP_v2'}], 'number': 1}, {'id': 143696, 'service_name': 'glpar', 'deployment_mode': 'server', 'deploy_by': 0, 'restart_by': 1, 'service_list': [{'id': 2348757, 'name': 'glp01-g02-gar02', 'domain': 'dev.glip.net', 'port': 3022, 'deployment_type': 'Auto', 'in_pool': True, 'in_pool_deployed': True, 'state': 'started', 'release': 'GLIP-GAR', 'monitoring_version_tag': 'GAR_v2', 'deployed_release': {'name': '', 'branch': 'release-1.16.x', 'build': '2021-06-14_21-26-52-v1.16.0', 'subcomponents': {}}, 'configured_release': {'name': 'GLIP-GAR', 'branch': 'release-1.16.x', 'build': '2021-06-14_21-26-52-v1.16.0', 'subcomponents': {}}, 'deployed_monitoring_version_tag': 'GAR_v2'}], 'number': 1}, {'id': 143668, 'service_name': 'glpin', 'deployment_mode': 'external', 'deploy_by': 1, 'restart_by': 1, 'service_list': [{'id': 2346590, 'name': 'glp01-g02-gin01', 'domain': 'glp-ft2-aws.svc.c01.eks01.k8s.aws11.dev.glip.net', 'port': 3014, 'deployment_type': 'Auto', 'in_pool': True, 'in_pool_deployed': True, 'state': 'started', 'release': 'GLP-FT2-AWS-glpin-1', 'deployed_release': {'name': '', 'branch': 'develop', 'build': '2021-08-03_08-40-04-47b4b31', 'subcomponents': {}}, 'configured_release': {'name': 'GLP-FT2-AWS-glpin-1', 'branch': 'develop', 'build': '2021-08-03_08-40-04-47b4b31', 'subcomponents': {}}}], 'number': 1}, {'id': 143698, 'service_name': 'glpmb', 'deployment_mode': 'server', 'deploy_by': 0, 'restart_by': 1, 'service_list': [{'id': 2348759, 'name': 'glp01-g02-gmb02', 'domain': 'dev.glip.net', 'port': 3000, 'deployment_type': 'Auto', 'in_pool': True, 'in_pool_deployed': True, 'state': 'started', 'release': 'GLP-FT2-AWS-glpmb-1', 'monitoring_version_tag': 'GMB_v1', 'deployed_release': {'name': '', 'branch': 'release-1.2.0', 'build': '2020-11-05_15-56-45-v1.2.0', 'subcomponents': {}}, 'configured_release': {'name': 'GLP-FT2-AWS-glpmb-1', 'branch': 'release-1.2.0', 'build': '2020-11-05_15-56-45-v1.2.0', 'subcomponents': {}}, 'deployed_monitoring_version_tag': 'GMB_v1'}], 'number': 1}, {'id': 143666, 'service_name': 'glpw', 'deployment_mode': 'external', 'deploy_by': 1, 'restart_by': 1, 'service_list': [{'id': 2346588, 'name': 'glp01-g02-gpw01', 'domain': 'glp-ft2-aws.svc.c01.eks01.k8s.aws11.dev.glip.net', 'port': 3018, 'deployment_type': 'Auto', 'in_pool': True, 'in_pool_deployed': True, 'state': 'started', 'release': 'GLP-FT2-AWS-glpw-2', 'deployed_release': {'name': '', 'branch': 'develop', 'build': '2021-08-03_11-22-19-2de83ae', 'subcomponents': {}}, 'configured_release': {'name': 'GLP-FT2-AWS-glpw-2', 'branch': 'develop', 'build': '2021-08-03_11-22-19-2de83ae', 'subcomponents': {}}}], 'number': 2}, {'id': 45401, 'service_name': 'gns', 'deployment_mode': 'server', 'deploy_by': 0, 'restart_by': 1, 'service_list': [{'id': 355640, 'name': 'aws11-g07-uds05', 'domain': 'dev.glip.net', 'port': 80, 'deployment_type': 'Auto', 'in_pool': True, 'in_pool_deployed': True, 'state': 'started', 'release': 'glp-ci-demo_nodejs', 'monitoring_version_tag': 'GNS_v12', 'deployed_release': {'name': '', 'branch': 'GLIP-32768', 'build': '2021-11-17_17-56-36_d353012f1b29ea13', 'subcomponents': {}}, 'configured_release': {'name': 'glp-ci-demo_nodejs', 'branch': None, 'build': '- Latest -', 'subcomponents': {}}, 'deployed_monitoring_version_tag': 'GNS_v12'}], 'number': 1}, {'id': 45402, 'service_name': 'gps', 'deployment_mode': 'server', 'deploy_by': 0, 'restart_by': 1, 'service_list': [{'id': 355641, 'name': 'aws11-g07-uds05', 'domain': 'dev.glip.net', 'port': 80, 'deployment_type': 'Auto', 'in_pool': True, 'in_pool_deployed': True, 'state': 'started', 'release': 'glp-ci-demo_nodejs', 'monitoring_version_tag': 'GPS_v12', 'deployed_release': {'name': '', 'branch': 'GLIP-32768', 'build': '2021-11-17_17-56-36_d353012f1b29ea13', 'subcomponents': {}}, 'configured_release': {'name': 'glp-ci-demo_nodejs', 'branch': None, 'build': '- Latest -', 'subcomponents': {}}, 'deployed_monitoring_version_tag': 'GPS_v12'}], 'number': 1}, {'id': 45403, 'service_name': 'gss', 'deployment_mode': 'server', 'deploy_by': 0, 'restart_by': 1, 'service_list': [{'id': 135002, 'name': 'aws11-g07-uds06', 'domain': 'dev.glip.net', 'port': 9200, 'deployment_type': 'Undeploy', 'in_pool': False, 'in_pool_deployed': True, 'state': 'started', 'deployed_release': {'name': '', 'branch': '3.0.1639-ads', 'build': '2016-10-11_13-44-06', 'subcomponents': {}}}], 'number': 1}, {'id': 143699, 'service_name': 'gst', 'deployment_mode': 'server', 'deploy_by': 0, 'restart_by': 1, 'service_list': [{'id': 2348760, 'name': 'glp01-g02-gst02', 'domain': 'dev.glip.net', 'port': 8080, 'deployment_type': 'Auto', 'in_pool': True, 'in_pool_deployed': True, 'state': 'started', 'release': 'GLP-FT2-AWS-gst-1', 'monitoring_version_tag': 'GST_v1', 'deployed_release': {'name': '', 'branch': 'release-1.0.x', 'build': '2020-08-27_18-08-02-v1.0.5', 'subcomponents': {}}, 'configured_release': {'name': 'GLP-FT2-AWS-gst-1', 'branch': 'release-1.0.x', 'build': '2020-08-27_18-08-02-v1.0.5', 'subcomponents': {}}, 'deployed_monitoring_version_tag': 'GST_v1'}], 'number': 1}, {'id': 45404, 'service_name': 'gsx', 'deployment_mode': 'server', 'deploy_by': 0, 'restart_by': 1, 'service_list': [{'id': 355642, 'name': 'aws11-g07-uds05', 'domain': 'dev.glip.net', 'port': 443, 'deployment_type': 'Auto', 'in_pool': True, 'in_pool_deployed': True, 'state': 'started', 'release': 'glp-ci-demo_nodejs', 'monitoring_version_tag': 'GSX_v15', 'deployed_release': {'name': '', 'branch': 'GLIP-32768', 'build': '2021-11-17_17-56-36_d353012f1b29ea13', 'subcomponents': {}}, 'configured_release': {'name': 'glp-ci-demo_nodejs', 'branch': None, 'build': '- Latest -', 'subcomponents': {}}, 'deployed_monitoring_version_tag': 'GSX_v15'}], 'number': 1}, {'id': 45405, 'service_name': 'gzs', 'deployment_mode': 'server', 'deploy_by': 0, 'restart_by': 1, 'service_list': [{'id': 355643, 'name': 'aws11-g07-uds05', 'domain': 'dev.glip.net', 'port': 443, 'deployment_type': 'Auto', 'in_pool': True, 'in_pool_deployed': True, 'state': 'started', 'release': 'glp-ci-demo_nodejs', 'monitoring_version_tag': 'GPS_v12', 'deployed_release': {'name': '', 'branch': 'GLIP-32768', 'build': '2021-11-17_17-56-36_d353012f1b29ea13', 'subcomponents': {}}, 'configured_release': {'name': 'glp-ci-demo_nodejs', 'branch': None, 'build': '- Latest -', 'subcomponents': {}}, 'deployed_monitoring_version_tag': 'REC_v1'}], 'number': 1}, {'id': 45406, 'service_name': 'mgs', 'deployment_mode': 'server', 'deploy_by': 0, 'restart_by': 1, 'service_list': [{'id': 135005, 'name': 'aws11-g07-uds06', 'domain': 'dev.glip.net', 'port': 27017, 'deployment_type': 'Undeploy', 'in_pool': False, 'in_pool_deployed': True, 'state': 'started', 'deployed_release': {'name': '', 'branch': '3.0.1639-ads', 'build': '2016-10-11_13-44-06', 'subcomponents': {}}}], 'number': 1}, {'id': 45407, 'service_name': 'mis', 'deployment_mode': 'server', 'deploy_by': 0, 'restart_by': 1, 'service_list': [{'id': 355644, 'name': 'aws11-g07-uds05', 'domain': 'dev.glip.net', 'port': 443, 'deployment_type': 'Auto', 'in_pool': True, 'in_pool_deployed': True, 'state': 'started', 'release': 'glp-ci-demo_nodejs', 'monitoring_version_tag': 'MIS_V16', 'deployed_release': {'name': '', 'branch': 'GLIP-32768', 'build': '2021-11-17_17-56-36_d353012f1b29ea13', 'subcomponents': {}}, 'configured_release': {'name': 'glp-ci-demo_nodejs', 'branch': None, 'build': '- Latest -', 'subcomponents': {}}, 'deployed_monitoring_version_tag': 'MIS_v15'}], 'number': 1}, {'id': 45408, 'service_name': 'rsm', 'deployment_mode': 'server', 'deploy_by': 0, 'restart_by': 1, 'service_list': [{'id': 135007, 'name': 'aws11-g07-uds06', 'domain': 'dev.glip.net', 'port': 6379, 'deployment_type': 'Undeploy', 'in_pool': False, 'in_pool_deployed': True, 'state': 'started', 'deployed_release': {'name': '', 'branch': '3.0.1639-ads', 'build': '2016-10-11_13-44-06', 'subcomponents': {}}}], 'number': 1}]}}

from_environment = 'https://ads.lab.nordigy.ru/environment-api/api/GLP-FT2-AWS/POP1/CL/?verbose=2'.split('/')[5].upper()
to_environment = 'https://ads.ringcentral.com/environment-api/api/Glip-PRO/POP1/CL/?verbose=2'.split('/')[5].upper()

def extract_pod_from_env():

    from_env = 'https://ads.lab.nordigy.ru/environment-api/api/GLP-FT2-AWS/POP1/CL/?verbose=2'
    # from_env = 'https://ads.lab.nordigy.ru/environment-api/api/GLP-FT1-AWS/POP1/CL/?verbose=2'
    # from_env = 'https://ads.lab.nordigy.ru/environment-api/api/GLP-FT3-AWS/POP1/CL/?verbose=2'
    # from_env = 'https://ads.lab.nordigy.ru/environment-api/api/GLP-FLL-AWS/POP1/CL/?verbose=2'

    from_environment = from_env.split('/')[5].upper()   
    from_response = requests.get(from_env)
    
    print (type(from_response.status_code))              
    if from_response.status_code == 200:
        print ("I am here")
        from_resp = json.loads(from_response.content)
    else:
        print ("From environment API request invalid" + from_response.status_code)
    
    return from_resp

def extract_pod_to_env():

    to_env = 'https://ads.ringcentral.com/environment-api/api/Glip-PRO/POP1/CL/?verbose=2'
    
    to_environment = to_env.split('/')[5].upper()
    to_response = requests.get(to_env)
    
    print (type(to_response.status_code))                 
    if to_response.status_code == 200:
        to_resp = json.loads(to_response.content)
    else:
        print ("From environment API request invalid" + to_response.status_code)
    
    return to_resp

def parse_pod_output(pod_data):

    output = []

    for row in range(len(pod_data["pod"]["group_list"])):
        out_temp = []
        out_temp.append(pod_data["pod"]["group_list"][row]["service_name"])
        if "deployed_release" in pod_data["pod"]["group_list"][row]["service_list"][0]:
            out_temp.append(pod_data["pod"]["group_list"][row]["service_list"][0]["deployed_release"]["branch"])
            out_temp.append(pod_data["pod"]["group_list"][row]["service_list"][0]["deployed_release"]["build"])
        else:
            out_temp.append(" ")
            out_temp.append(" ")
        
        output.append(out_temp)
    
    return output

def compare_from_to_pod(from_resp_parse, to_resp_parse):
    
    for fpod in range(len(from_resp_parse)):
        
        (to_branch, to_build) = get_pod_data(from_resp_parse[fpod][0], to_resp_parse)
        
        if to_branch == 'not found':
            missing_in_to_env.append(from_resp_parse[fpod])
        else:
            if ((to_branch != from_resp_parse[fpod][1]) or (to_build != from_resp_parse[fpod][2])):
                mismatch_temp = []
                mismatch_temp.append(from_resp_parse[fpod][0])
                mismatch_temp.append(from_resp_parse[fpod][1])
                mismatch_temp.append(from_resp_parse[fpod][2])
                mismatch_temp.append(to_branch)
                mismatch_temp.append(to_build)
                mismatch_in_to_env.append(mismatch_temp)            

    for lpod in range(len(to_resp_parse)):
        
        (from_branch, from_build) = get_pod_data(to_resp_parse[lpod][0], from_resp_parse)
        
        if from_branch == 'not found':
            missing_in_from_env.append(to_resp_parse[lpod])

    return (mismatch_in_to_env, missing_in_to_env, missing_in_from_env)

def get_pod_data(service_name, resp_parse):
    
    name_found = False
    release_branch = ''
    release_build = ''
    
    for pod in range(len(resp_parse)):        
        if name_found == False:
            if resp_parse[pod][0] == service_name:
                name_found = True
                release_branch = resp_parse[pod][1]
                release_build = resp_parse[pod][2]
    
    if name_found == False:
        release_branch = 'not found'
        release_build = 'not found'

    return (release_branch, release_build)

def write_output_report(from_environment, to_environment, mismatch_in_to_env, missing_in_to_env, missing_in_from_env):
    
    file = open("POD_OutFile.txt", "w")
    
    file.write("                                     COMPARE OF " + from_environment + " AND " + to_environment + " ENVIRONMENT - MISMATCH BETWEEN ENVIRONMENT                        \n")
    file.write("                          <====================================================================================================>                                      \n")
    file.write("                                               " + from_environment + "                                                        " + to_environment + "                 \n")
    file.write(" Service name            release-branch                         release-build                    release-branch                            release-build              \n")
    file.write(" ------------   ------------------------------   ----------------------------------------   ------------------------------   ---------------------------------------- \n")
        
    if len(mismatch_in_to_env) == 0:
        file.write(" No mismatch found between From and To environment")
    else:
        for row in range(len(mismatch_in_to_env)):
            file.write(" " + 
                       mismatch_in_to_env[row][0].ljust(15) + 
                       mismatch_in_to_env[row][1].ljust(33) + 
                       mismatch_in_to_env[row][2].ljust(43) + 
                       mismatch_in_to_env[row][3].ljust(33) + 
                       mismatch_in_to_env[row][4].ljust(43) +  "\n")    
    file.write('\n' * 2)
    
    file.write(" COMPARE OF " + from_environment + " AND " + to_environment + " ENVIRONMENT - MISSING FROM " + from_environment + " ENVIRONMENT      \n")
    file.write(" <=====================================================================================>                                             \n")
    file.write("                                                                                                                                     \n")
    file.write(" Service name            release-branch                        release-build                                                         \n")
    file.write(" ------------   ------------------------------   ----------------------------------------                                            \n")
        
    if len(missing_in_from_env) == 0:
        file.write(" No missing services found between From and To environment")
    else:
        for row in range(len(missing_in_from_env)):
            file.write(" " + 
                       missing_in_from_env[row][0].ljust(15) + 
                       missing_in_from_env[row][1].ljust(33) + 
                       missing_in_from_env[row][2].ljust(43) + "\n")        
    file.write('\n' * 2)
    
    file.write(" COMPARE OF " + from_environment + " AND " + to_environment + " ENVIRONMENT - MISSING FROM " + to_environment + " ENVIRONMENT        \n")
    file.write(" <=====================================================================================>                                             \n")
    file.write("                                                                                                                                     \n")
    file.write(" Service name            release-branch                        release-build                                                         \n")
    file.write(" ------------   ------------------------------   ----------------------------------------                                            \n")
        
    if len(missing_in_to_env) == 0:
        file.write(" No missing services found between From and To environment")
    else:
        for row in range(len(missing_in_to_env)):
            file.write(" " + 
                       missing_in_to_env[row][0].ljust(15) + 
                       missing_in_to_env[row][1].ljust(33) + 
                       missing_in_to_env[row][2].ljust(33) + "\n")    

def main():
    
#    from_resp = extract_pod_from_env()
#    to_resp = extract_pod_to_env()
    
    from_resp_parse = parse_pod_output(from_resp)
    to_resp_parse = parse_pod_output(to_resp)
    
    (mismatch_in_to_env, missing_in_to_env, missing_in_from_env) = compare_from_to_pod(from_resp_parse, to_resp_parse)
    write_output_report(from_environment, to_environment, mismatch_in_to_env, missing_in_to_env, missing_in_from_env)
    
if __name__ == "__main__":
    main()





